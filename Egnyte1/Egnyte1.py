import glob, os
import json
import flatten_json
import MySQLdb

from collections import Counter


def parse_files(path, values):
    print("Reading files")
    for file in os.listdir(path):
        with open("/".join([path,file]), encoding='utf-8') as f:
            print("Reading file {1}", file)
            values.extend([x.strip() for x in f.readlines()])
    print("finished reading files")


def create_json(values):
    print("Creating jsons")
    jsons = [json.loads(x) for x in values]
    print("jsons created")
    return jsons


def flatten(jsons):
    print("flattening jsons")
    flatten_jsons = [flatten_json.flatten_json(x) for x in jsons]
    print("jsons flattened")
    return flatten_jsons


def count_data(parameter, jsons):
    return Counter([json.get(parameter) for json in jsons])


def prepare_data(row):
    new_row = {}
    for field, value in row.items():
        if value == None:
            new_row.update({field:"0"})
        elif isinstance(value, int):
            new_row.update({field:str(value)})
        elif isinstance(value, float):
            new_row.update({field:str(int(value))})
        else:
            new_row.update({field:value})
    return new_row


def db_connection(data):
    print("connecting to database")
    try:
        conn=MySQLdb.connect(host="localhost",user="root",passwd="root",db="egnyte")
        x = conn.cursor()
        for row in data:
            pass
            prepared_row = prepare_data(row)
            string_row = "' , '".join(tuple(prepared_row.values()))
            string_row = "".join(["INSERT INTO egnyte VALUES('", string_row,"')"])
            x.execute (string_row)
        conn.commit()
    finally:
        print("closing connection")
        conn.close()

if __name__ == '__main__':
    values=[]
    jsons = []
    path = 'Data/out'
    parse_files(path, values)
    jsons = create_json(values)
    flatten_jsons = flatten(jsons)
    
    listofkeys = [x for x in flatten_jsons[0]]
    calculated_amounts = [count_data(x, flatten_jsons) for x in listofkeys]

    db_connection(flatten_jsons)
    pass